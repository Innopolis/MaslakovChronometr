/**
 * Created by sa on 09.02.17.
 */
public class Main {
    static volatile int cntTime5 = 0;
    static volatile int cntTime7 = 0;
    public static Object lock = new Object();
    public static void main(String[] args) {
        Clock clock = new Clock();
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(1000);
                        System.out.print("one second: ");
                        synchronized (lock) {
                            clock.printTime();
                            lock.notifyAll();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    synchronized (lock){
                        while (cntTime5 < 5) {
                            cntTime5++;
                            try {
                                lock.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        cntTime5 = 0;
                        System.out.print("FIVE SECOND: ");
                        clock.printTime();
                    }
                }
            }
        });

        Thread thread3 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    synchronized (lock){
                        while (cntTime7 < 7) {
                            cntTime7++;
                            try {
                                lock.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        cntTime7 = 0;
                        System.out.print("SEVEN second: ");
                        clock.printTime();
                    }
                }
            }
        });

        thread1.start();
        thread2.start();
        thread3.start();

    }
}
